import 'dart:io';

typedef InternetSuccess = void Function();
typedef InternetFailure = void Function();


Future<void> checkInternet({
  InternetSuccess? internetSuccess,
  InternetFailure? internetFailure,
}) async {
  try {
    final result = await InternetAddress.lookup('example.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      internetSuccess!();
    }
  } on SocketException catch (_) {
    internetFailure!();
  }
}
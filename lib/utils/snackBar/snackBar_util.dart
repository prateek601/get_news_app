import 'package:get/get.dart';

class SnackBarUtil {
  SnackBarUtil._privateConstructor();

  static void showSnackBar({required String message}) {
    Get.rawSnackbar(message: message);
  }
}
import 'package:get/get.dart';
import 'package:get_news_app/app/data/network/network_requester.dart';

class BaseRepositry {
  NetworkRequester get controller => GetInstance().find<NetworkRequester>();
}
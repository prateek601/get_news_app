import 'package:get/get.dart';

import '../modules/detailed_news/bindings/detailed_news_binding.dart';
import '../modules/detailed_news/views/detailed_news_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/search/bindings/search_binding.dart';
import '../modules/search/views/search_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH,
      page: () => SearchView(),
      binding: SearchBinding(),
    ),
    GetPage(
      name: _Paths.DETAILED_NEWS,
      page: () => DetailedNewsView(),
      binding: DetailedNewsBinding(),
    ),
  ];
}

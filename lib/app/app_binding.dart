import 'package:get/get.dart';
import 'package:get_news_app/app/data/network/network_requester.dart';
import 'package:get_news_app/app/data/repository/news_repository.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(NetworkRequester(), permanent: true);
    Get.put(NewsRepository(), permanent: true);
  }
}
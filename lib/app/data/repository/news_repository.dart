import 'package:get_news_app/app/data/models/dto/news.dart';
import 'package:get_news_app/app/data/models/dto/response.dart';
import 'package:get_news_app/base/base_repository.dart';
import 'package:get_news_app/utils/helper/exception_handler.dart';

class NewsRepository extends BaseRepositry {
   Future <RepoResponse<News>> fetchNews ({String? path, Map<String, dynamic>? queryParams}) async {
     final response =
         await controller.get(path: path!,query: queryParams!);

     return response is APIException
         ? RepoResponse(error: response)
         : RepoResponse(data: News.fromJson(response));
   }
}
class Strings {
  Strings._privateConstructor();

  static const homeAppBarTitle = 'MyNEWS';
  static const locationBottomSheetHeading = 'Select Location';
  static const apply = 'Apply';
  static const location = 'LOCATION';
  static const filterToolTip = 'News source';
  static const searchPlaceholder = 'Search for news, topics...';
  static const topHeadlines = 'Top Headlines';
  static const sort = 'Sort: ';
  static const newsSourceBottomSheetHeading = 'Filter by news sources';
  static const applyFilter = 'Apply Filter';
  static const noResultsAvailable = 'No results available';
  static const defaultLocation = 'India';
  static const defaultSortPreference = 'Newest';
  static const defaultCountryCode = 'in';
  static const searchScreenAppBarTitle = 'Search';
  static const seeFullStory = 'See full story';
  static const noInternetMessage = 'No internet Connection!';
  static const tryAgain = 'Try again';
}

class ErrorMessages {
  ErrorMessages._privateConstructor();

  static const noInternet = 'Please check your internet connection';
  static const connectionTimeout = 'Please check your internet connection';
  static const networkGeneral = 'Something went wrong. Please try again later.';
  static const selectNewsSource = 'Select news sources first and then apply sort preference';
  static const launchUrlError = 'cannot launch website';
}

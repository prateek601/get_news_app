import 'package:flutter/material.dart';

class Timeouts {
  Timeouts._privateConstructor();

  static const CONNECT_TIMEOUT = 10000;
  static const RECEIVE_TIMEOUT = 10000;
}

class GlobalKeys {
  GlobalKeys._privateConstructor();

  static final navigationKey = GlobalKey<NavigatorState>();
}

const List<String> countryList = ['Australia', 'USA', 'India', 'South Africa', 'France', 'Germany'];
const List<String> newsSources = ['ABC News', 'Business Insider', 'CNN', 'The Hindu', 'Google News (India)', 'Bloomberg'];
const List<String> sortPreference = ['Popular', 'Newest', 'Relevant'];

const Map<String, String> countryMap = {
  'Australia': 'au',
  'USA': 'us',
  'India': 'in',
  'South Africa': 'za',
  'France': 'fr',
  'Germany': 'de'
};

const Map<String, String> newsSourceMap = {
  'ABC News': 'abc-news',
  'Business Insider': 'business-insider',
  'CNN': 'cnn',
  'The Hindu': 'the-hindu',
  'Google News (India)': 'google-news-in',
  'Bloomberg': 'bloomberg'
};

const Map<String, String> sortPreferenceMap = {
  'Popular': 'popularity',
  'Newest': 'publishedAt',
  'Relevant': 'relevancy'
};

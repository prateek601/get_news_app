class URLs {
  URLs._privateConstructor();

  static const locationNews = "top-headlines";
  static const everything = 'everything';
}

class QueryParameters {
  QueryParameters._privateConstructor();

  static const country = 'country';
  static const sources = 'sources';
  static const sortBy = 'sortBy';
  static const pageSize = 'pageSize';
  static const page = 'page';
  static const searchQuery = 'q';
}

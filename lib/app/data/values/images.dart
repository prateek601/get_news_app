class Images {
  Images._privateConstructor();

  static const filter = 'assets/images/filter.png';
  static const noInternet = 'assets/images/no_internet.png';
}
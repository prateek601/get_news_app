class Env {
  Env._privateConstructor();

  static const title = "Sample Project";

  static const baseURL = "https://newsapi.org/v2/";
}

import 'package:get/get.dart';

import '../controllers/detailed_news_controller.dart';

class DetailedNewsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailedNewsController>(
      () => DetailedNewsController(),
    );
  }
}

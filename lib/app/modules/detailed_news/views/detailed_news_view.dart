import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_news_app/app/data/models/dto/news.dart';
import 'package:get_news_app/app/data/values/strings.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';
import 'package:get_news_app/utils/snackBar/snackBar_util.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../controllers/detailed_news_controller.dart';

class DetailedNewsView extends GetView<DetailedNewsController> {
  final Article article = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primaryColor1,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Image.network(
                article.urlToImage,
                height: MediaQuery.of(context).size.height * 0.35,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              Positioned.fill(
                bottom: 20,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: RichText(
                        text: TextSpan(
                            text: article.title,
                            style: Styles.tsWhiteRegular14),
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, bottom: 30, top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: article.source.name,
                    style: Styles.tsPrimaryColor2ItalicBold16,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                RichText(
                  text: TextSpan(
                    text: publishedAt(),
                    style: Styles.tsGrey700Regular14,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: RichText(
                    text: TextSpan(
                      text: article.description,
                      style: const TextStyle(
                        wordSpacing: 1,
                        color: AppColors.black,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 17,
                ),
                InkWell(
                  onTap: () async {
                    if (article.url != null) {
                      String url = article.url!;
                      try {
                        if (!await launch(url, forceWebView: true)) {
                          throw ('cannot launch url: $url');
                        }
                      } catch (e) {
                        SnackBarUtil.showSnackBar(
                            message: ErrorMessages.launchUrlError);
                      }
                    } else {
                      SnackBarUtil.showSnackBar(
                          message: ErrorMessages.launchUrlError);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 3),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: Strings.seeFullStory,
                            style: Styles.tsPrimaryColor1Bold14
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          size: 20,
                          color: AppColors.primaryColor1,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  String publishedAt() {
    DateTime dateTime = DateTime.parse(article.publishedAt).toLocal();
    String formattedDate = DateFormat('dd MMM, yyyy').format(dateTime);
    String formattedTime = DateFormat('h:mm a').format(dateTime);
    String publishedAt = formattedDate + ' at ' + formattedTime;
    return publishedAt;
  }
}

import 'package:flutter/material.dart';
import 'package:get_news_app/app/data/values/constants.dart';
import 'package:get_news_app/app/modules/home/controllers/home_controller.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/theme/styles.dart';

class CountryListView extends GetView<HomeController> {
  const CountryListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: countryList.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return Obx(
          () => Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                  text: countryList[index],
                  style: controller.tempLocation.value == countryList[index]
                      ? Styles.ts14PrimaryColor1 : Styles.tsBlack14
                )
              ),
              Radio(
                value: true,
                groupValue: controller.tempLocation.value == countryList[index],
                activeColor: AppColors.primaryColor1,
                onChanged: (value) {
                  controller.tempLocation.value = countryList[index];
                },
              )
            ],
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_news_app/app/data/values/constants.dart';
import 'package:get_news_app/app/data/values/images.dart';
import 'package:get_news_app/app/data/values/strings.dart';
import 'package:get_news_app/app/modules/home/views/bottom_sheet.dart';
import 'package:get_news_app/app/modules/home/views/country_list_view.dart';
import 'package:get_news_app/app/modules/home/views/sources_list_view.dart';
import 'package:get_news_app/app/routes/app_pages.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';
import 'package:get_news_app/utils/snackBar/snackBar_util.dart';
import 'package:get_news_app/widgets/news_card_list.dart';
import 'package:get_news_app/widgets/no_internet_view.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primaryColor1,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            RichText(
              text: TextSpan(
                text: Strings.homeAppBarTitle,
                style: Styles.tsSecondaryColor1Regular18,
              ),
            ),
            InkWell(
              onTap: () => locationBottomSheet(context),
              child: selectLocation(),
            )
          ],
        ),
      ),
      floatingActionButton: Obx(
            () {
          if (controller.internetConnected.value) {
            return FloatingActionButton(
              onPressed: () => newsSourcesBottomSheet(context),
              backgroundColor: AppColors.primaryColor1,
              child: Image.asset(
                Images.filter,
                height: 20,
                width: 20,
              ),
              tooltip: Strings.filterToolTip,
            );
          } else {
            return Container();
          }
        },
      ),
      body: myBody(),
    );
  }

  Widget myBody() {
    return Obx(() {
      if (controller.internetConnected.value) {
        return SingleChildScrollView(
          controller: controller.scrollController,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 60),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 30),
                  child: InkWell(
                    onTap: () {
                      Get.toNamed(Routes.SEARCH);
                    },
                    child: Container(
                      height: 40,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: AppColors.grey300,
                          borderRadius: BorderRadius.circular(5)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                              text: TextSpan(
                                text: Strings.searchPlaceholder,
                                style: Styles.tsGrey500Regular12
                              ),
                            ),
                            Icon(
                              Icons.search,
                              color: AppColors.grey700,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: Strings.topHeadlines,
                          style: Styles.tsGrey700Bold16
                        ),
                      ),
                      Row(
                        children: [
                          RichText(
                            text: TextSpan(
                              text: Strings.sort,
                              style: Styles.tsGrey700Regular12
                            ),
                          ),
                          myDropDown(),
                        ],
                      )
                    ],
                  ),
                ),
                Obx(() {
                  if (controller.isLoading.value) {
                    return CircularProgressIndicator();
                  } else {
                    if (controller.articles.isEmpty &&
                        controller.isError.value) {
                      return RichText(
                        text: TextSpan(text: ErrorMessages.networkGeneral),
                      );
                    } else {
                      return NewsCardsList(articles: controller.articles);
                    }
                  }
                }),
                Obx(() {
                  if (controller.reachedMaxScrollExtent.value) {
                    return CircularProgressIndicator();
                  } else {
                    return Container();
                  }
                })
              ],
            ),
          ),
        );
      } else {
        return NoInternet(
          onButtonTap: () {
            controller.makeFetchRequest();
          },
        );
      }
    });
  }

  void locationBottomSheet(BuildContext context) {
    controller.tempLocation.value = controller.location.value;
    bottomSheet(
      context: context,
      heading: Strings.locationBottomSheetHeading,
      listWidget: CountryListView(),
      buttonName: Strings.apply,
      onButtonTap: () {
        controller.newsSources.clear();
        controller.sortPreference.value = Strings.defaultSortPreference;
        controller.location.value = controller.tempLocation.value;
        controller.fetchData();
      },
    );
  }

  void newsSourcesBottomSheet(BuildContext context) {
    controller.tempNewsSources.value = controller.newsSources.toList();
    bottomSheet(
      context: context,
      heading: Strings.newsSourceBottomSheetHeading,
      listWidget: SourcesListView(),
      buttonName: Strings.applyFilter,
      onButtonTap: () {
        controller.newsSources.value = controller.tempNewsSources.toList();
        controller.fetchData();
      },
    );
  }

  Widget selectLocation() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        RichText(
          text: TextSpan(
            text: Strings.location,
            style: Styles.tsSecondaryColor1Regular10
          ),
        ),
        Row(
          children: [
            const Icon(
              Icons.location_on,
              size: 16,
            ),
            Obx(
                  () =>
                  RichText(
                    text: TextSpan(
                      text: controller.location.value,
                      style: Styles.tsSecondaryColor1Regular12
                    ),
                  ),
            ),
          ],
        )
      ],
    );
  }

  Widget myDropDown() {
    return Obx(
          () =>
          DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              items: sortPreference.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: RichText(
                    text: TextSpan(
                      text: value,
                      style: Styles.tsGrey500Regular14
                    ),
                  ),
                );
              }).toList(),
              onChanged: (value) {
                if (controller.newsSources.isNotEmpty) {
                  controller.sortPreference.value = value!;
                  controller.fetchData();
                } else {
                  SnackBarUtil.showSnackBar(
                    message: ErrorMessages.selectNewsSource,
                  );
                }
              },
              borderRadius: BorderRadius.circular(10),
              value: controller.sortPreference.value,
              isDense: true,
            ),
          ),
    );
  }

}

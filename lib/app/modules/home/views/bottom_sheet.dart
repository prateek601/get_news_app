import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';

typedef OnButtonTap = void Function();

Future<void> bottomSheet(
    {required BuildContext context,
    required String heading,
    required Widget listWidget,
    required String buttonName,
    required OnButtonTap onButtonTap}) {
  return Get.bottomSheet(
    SizedBox(
      height: MediaQuery.of(context).size.height * 0.65,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Container(
                height: 4,
                width: 40,
                decoration: BoxDecoration(
                  color: AppColors.secondaryColor2,
                  borderRadius: BorderRadius.circular(2),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: RichText(
                    text: TextSpan(
                        text: heading, style: Styles.tsPrimaryColor2Bold14),
                  ),
                )
              ],
            ),
            Container(
              height: 1,
              color: AppColors.secondaryColor2,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  listWidget,
                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 20),
                    child: InkWell(
                      onTap: () {
                        Get.back();
                        onButtonTap();
                      },
                      child: Container(
                        height: 50,
                        width: 150,
                        decoration: BoxDecoration(
                          color: AppColors.primaryColor1,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              text: buttonName,
                              style: Styles.tsSecondaryColor2Regular14,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(5),
        topRight: Radius.circular(5),
      ),
    ),
    isScrollControlled: true,
    backgroundColor: AppColors.secondaryColor1,
  );
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/data/values/constants.dart';
import 'package:get_news_app/app/modules/home/controllers/home_controller.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';

class SourcesListView extends GetView<HomeController> {
  const SourcesListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: newsSources.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return Obx(
          () => Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                  text: newsSources[index],
                  style: controller.tempNewsSources.contains(newsSources[index]) ?
                      Styles.ts14PrimaryColor1 : Styles.tsBlack14
                ),
              ),
              Checkbox(
                checkColor: AppColors.white,
                value: controller.tempNewsSources.contains(newsSources[index]),
                onChanged: (bool? value) {
                  if (value == true) {
                    controller.tempNewsSources.add(newsSources[index]);
                  } else {
                    controller.tempNewsSources.remove(newsSources[index]);
                  }
                },
              )
            ],
          ),
        );
      },
    );
  }
}

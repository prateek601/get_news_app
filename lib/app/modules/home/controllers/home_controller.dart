import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/data/models/dto/news.dart';
import 'package:get_news_app/app/data/models/dto/response.dart';
import 'package:get_news_app/app/data/repository/news_repository.dart';
import 'package:get_news_app/app/data/values/constants.dart';
import 'package:get_news_app/app/data/values/strings.dart';
import 'package:get_news_app/app/data/values/urls.dart';
import 'package:get_news_app/base/base_controller.dart';
import 'package:get_news_app/utils/helper/check_internet.dart';
import 'package:get_news_app/utils/helper/exception_handler.dart';

class HomeController extends BaseController<NewsRepository> {
  RxBool isLoading = true.obs;
  RxBool isError = false.obs;
  RxList articles = [].obs;
  RxString location = Strings.defaultLocation.obs;
  int totalResults = 0;
  int totalPages = 1;
  int pageSize = 8;
  int page = 1;
  RxBool reachedMaxScrollExtent = false.obs;
  RxString tempLocation = Strings.defaultLocation.obs;
  RxList newsSources = [].obs;
  RxList tempNewsSources = [].obs;
  RxString sortPreference = Strings.defaultSortPreference.obs;
  ScrollController scrollController = ScrollController();
  RxBool internetConnected = true.obs;
  late String path;
  Map<String, dynamic> queryParams = {};

  @override
  void onInit() {
    super.onInit();
    path = URLs.locationNews;
    queryParams[QueryParameters.country] = Strings.defaultCountryCode;
    makeFetchRequest();
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        fetchMoreData();
      }
    });
  }

  void fetchMoreData() {
    page++;
    if (page <= totalPages) {
      reachedMaxScrollExtent.value = true;
      makeFetchRequest();
    }
  }

  void fetchData() {
    isLoading.value = true;
    page = 1;
    if (newsSources.isEmpty) {
      String countryCode = countryMap[location.value]!;
      queryParams.clear();
      path = URLs.locationNews;
      queryParams[QueryParameters.country] = countryCode;
    } else {
      String sources = '';
      for (int i = 0; i < newsSources.length; i++) {
        String selectedSource = newsSourceMap[newsSources[i]]!;
        if (i == newsSources.length - 1) {
          sources = sources + selectedSource;
        } else {
          sources = sources + selectedSource + ',';
        }
      }
      String sortBy = sortPreferenceMap[sortPreference.value]!;
      queryParams.clear();
      path = URLs.everything;
      queryParams[QueryParameters.sources] = sources;
      queryParams[QueryParameters.sortBy] = sortBy;
    }
    makeFetchRequest();
  }

  Future<void> makeFetchRequest() async {
    String _pageSize = pageSize.toString();
    String _page = page.toString();
    queryParams[QueryParameters.pageSize] = _pageSize;
    queryParams[QueryParameters.page] = _page;

    checkInternet(
      internetFailure: () {
        internetConnected.value = false;
        if(!Get.isSnackbarOpen) {
          Get.rawSnackbar(message: ErrorMessages.noInternet);
        }
      },
      internetSuccess: () async {
        internetConnected.value = true;
        RepoResponse<News> response =
            await repository.fetchNews(path: path, queryParams: queryParams);
        if (response.data != null) {
          isError.value = false;
          News news = response.data!;
          if (reachedMaxScrollExtent.value) {
            articles.addAll(news.articles);
            reachedMaxScrollExtent.value = false;
          } else {
            articles.clear();
            articles.addAll(news.articles);
            totalResults = news.totalResults;
            totalPages = (totalResults / pageSize).ceil();
          }
        } else {
          isError.value = true;
          HandleError.handleError(response.error);
        }
        isLoading.value = false;
      }
    );

  }

  @override
  void onClose() {
    super.onClose();
    scrollController.dispose();
  }
}

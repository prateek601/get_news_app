import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_news_app/app/data/values/strings.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';
import 'package:get_news_app/widgets/news_card_list.dart';

import '../controllers/search_controller.dart';

class SearchView extends GetView<SearchController> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primaryColor1,
          title: RichText(
            text: TextSpan(
              text: Strings.searchScreenAppBarTitle,
              style: Styles.tsSecondaryColor1Regular18,
            ),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          controller: controller.scrollController,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 30),
                  child: TextField(
                    autofocus: true,
                    textInputAction: TextInputAction.search,
                    controller: controller.textEditingController,
                    cursorColor: AppColors.black,
                    style: const TextStyle(fontSize: 18),
                    decoration: InputDecoration(
                      suffixIcon: const Icon(Icons.search),
                      isDense: true,
                      hintText: Strings.searchPlaceholder,
                      hintStyle: const TextStyle(fontSize: 12),
                      fillColor: AppColors.secondaryColor2,
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(style: BorderStyle.none),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(style: BorderStyle.none),
                      ),
                    ),
                    onEditingComplete: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if (controller.textEditingController.text.isNotEmpty) {
                        controller.fetchSearchData();
                      }
                    },
                  ),
                ),
                Obx(() {
                  if (controller.isLoading.value) {
                    return CircularProgressIndicator();
                  } else {
                    if (controller.articles.isEmpty &&
                        controller.isError.value) {
                      return RichText(
                        text: TextSpan(
                          text: ErrorMessages.networkGeneral,
                        ),
                      );
                    } else if (controller.articles.isNotEmpty &&
                        controller.isError.value == false) {
                      return NewsCardsList(articles: controller.articles);
                    } else {
                      return Container();
                    }
                  }
                }),
                Obx(() {
                  if (controller.reachedMaxScrollExtent.value) {
                    return CircularProgressIndicator();
                  } else {
                    return Container();
                  }
                })
              ],
            ),
          ),
        ),
      ),
    );
  }
}

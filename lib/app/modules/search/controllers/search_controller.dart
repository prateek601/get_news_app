import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/data/models/dto/news.dart';
import 'package:get_news_app/app/data/models/dto/response.dart';
import 'package:get_news_app/app/data/repository/news_repository.dart';
import 'package:get_news_app/app/data/values/urls.dart';
import 'package:get_news_app/base/base_controller.dart';
import 'package:get_news_app/utils/helper/exception_handler.dart';

class SearchController extends BaseController<NewsRepository> {
  ScrollController scrollController = ScrollController();
  TextEditingController textEditingController = TextEditingController();
  int page = 1;
  int pageSize = 8;
  int totalPages = 1;
  RxList articles = [].obs;
  RxBool isLoading = false.obs;
  RxBool isError = false.obs;
  RxBool reachedMaxScrollExtent = false.obs;
  String path = '';
  Map<String, dynamic> queryParams = {};

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        fetchMoreData();
      }
    });
  }

  void fetchMoreData() {
    page++;
    if (page <= totalPages) {
      reachedMaxScrollExtent.value = true;
      makeFetchRequest();
    }
  }

  void fetchSearchData() {
    isLoading.value = true;
    page = 1;
    path = URLs.everything;
    queryParams[QueryParameters.searchQuery] = textEditingController.text;
    makeFetchRequest();
  }

  Future<void> makeFetchRequest() async {
    String _pageSize = pageSize.toString();
    String _page = page.toString();
    queryParams[QueryParameters.pageSize] = _pageSize;
    queryParams[QueryParameters.page] = _page;

    RepoResponse<News> response =
        await repository.fetchNews(path: path, queryParams: queryParams);
    if (response.data != null) {
      isError.value = false;
      News news = response.data!;
      if (reachedMaxScrollExtent.value) {
        articles.addAll(news.articles);
        reachedMaxScrollExtent.value = false;
      } else {
        articles.clear();
        articles.addAll(news.articles);
        int totalResults = news.totalResults;
        totalPages = (totalResults / pageSize).ceil();
      }
    } else {
      isError.value = true;
      print(response.error?.message ?? '');
      HandleError.handleError(response.error);
    }
    isLoading.value = false;
  }

  @override
  void onClose() {
    super.onClose();
    textEditingController.dispose();
    scrollController.dispose();
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/app_binding.dart';
import 'package:get_news_app/app/data/values/constants.dart';
import 'package:get_news_app/app/data/values/env.dart';
import 'package:get_news_app/app/routes/app_pages.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: Env.title,
      navigatorKey: GlobalKeys.navigationKey,
      debugShowCheckedModeBanner: false,
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      defaultTransition: Transition.fade,
      initialBinding: AppBinding(),
    );
  }
}
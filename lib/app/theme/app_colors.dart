import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor1 = Color(0xff0C54BE);
  static const Color primaryColor2 = Color(0xff303F60);
  static const Color secondaryColor1 = Color(0xffF5F9FD);
  static const Color secondaryColor2 = Color(0xffCED3DC);
  static const Color grey500 = Color(0xff9e9e9e);
  static const Color grey300 = Color(0xffe0e0e0);
  static const Color grey700 = Color(0xff616161);
  static const Color white = Color(0xffffffff);
  static const Color black = Color(0xff000000);
}
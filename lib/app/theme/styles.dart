import 'package:flutter/material.dart';

import 'app_colors.dart';

class Styles {
  Styles._privateConstructor();

  static const tsSecondaryColor1Regular18 = TextStyle(
    color: AppColors.secondaryColor1,
    fontWeight: FontWeight.w400,
    fontSize: 18,
  );

  static const tsSecondaryColor1Regular10 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: AppColors.secondaryColor1,
  );

  static const tsSecondaryColor1Regular12 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.secondaryColor1,
  );

  static const tsGrey500Regular12 = TextStyle(
    fontSize: 12,
    color: AppColors.grey500,
    fontWeight: FontWeight.w400,
  );

  static const tsGrey700Bold16 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    color: AppColors.grey700,
  );

  static const tsGrey700Regular12 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.grey700,
  );

  static const tsGrey500Regular14 = TextStyle(
    fontSize: 14,
    color: AppColors.grey500,
    fontWeight: FontWeight.w400,
  );

  static const tsBlackItalicBold16 = TextStyle(
    color: AppColors.black,
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.italic,
  );

  static const tsBlack14 = TextStyle(
    color: AppColors.black,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  static const tsPrimaryColor2Bold14 = TextStyle(
    fontSize: 14,
    color: AppColors.primaryColor2,
    fontWeight: FontWeight.w600,
  );

  static const tsSecondaryColor2Regular14 = TextStyle(
    fontSize: 14,
    color: AppColors.secondaryColor2,
    fontWeight: FontWeight.w400,
  );

  static const ts14PrimaryColor1 = TextStyle(
    color: AppColors.primaryColor1,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  static const tsWhiteRegular14 = TextStyle(
    color: AppColors.white,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  static const tsPrimaryColor2ItalicBold16 = TextStyle(
    fontSize: 16,
    fontStyle: FontStyle.italic,
    fontWeight: FontWeight.w600,
    color: AppColors.primaryColor2,
  );

  static const tsGrey700Regular14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w400, color: AppColors.grey700);

  static const tsPrimaryColor1Bold14 = TextStyle(
    color: AppColors.primaryColor1,
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );

  static const tsGrey700Regular18 = TextStyle(
    color: AppColors.grey700,
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );

  static const tsWhiteRegular16 = TextStyle(
    color: AppColors.white,
    fontSize: 16,
  );

}

import 'package:flutter/material.dart';
import 'package:get_news_app/app/data/values/images.dart';
import 'package:get_news_app/app/data/values/strings.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';

class NoInternet extends StatelessWidget {
  final GestureTapCallback? onButtonTap;

  const NoInternet({Key? key, required this.onButtonTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              Images.noInternet,
              height: 120,
              width: 120,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15, bottom: 20),
              child: RichText(
                text: TextSpan(
                  text: Strings.noInternetMessage,
                  style: Styles.tsGrey700Regular18,
                ),
              ),
            ),
            InkWell(
              onTap: onButtonTap,
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                  color: AppColors.primaryColor1,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      text: Strings.tryAgain,
                      style: Styles.tsWhiteRegular16,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}

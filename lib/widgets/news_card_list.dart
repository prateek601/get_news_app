import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_news_app/app/data/values/strings.dart';
import 'package:get_news_app/app/routes/app_pages.dart';
import 'package:get_news_app/app/theme/app_colors.dart';
import 'package:get_news_app/app/theme/styles.dart';

class NewsCardsList extends StatelessWidget {
  final List articles;

  const NewsCardsList({Key? key, required this.articles}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return articles.isEmpty
        ? Center(
            child: RichText(
              text: TextSpan(text: Strings.noResultsAvailable),
            ),
          )
        : ListView.builder(
            itemCount: articles.length,
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: InkWell(
                  onTap: () {
                    Get.toNamed(
                      Routes.DETAILED_NEWS,
                      arguments: articles[index],
                    );
                  },
                  child: Container(
                    height: 140,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: AppColors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                          text: articles[index].source.name,
                                          style: Styles.tsBlackItalicBold16,
                                        ),
                                        maxLines: 1,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      RichText(
                                        text: TextSpan(
                                            text: articles[index].title,
                                            style: Styles.tsBlack14),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                      ),
                                    ],
                                  ),
                                  RichText(
                                    text: TextSpan(
                                      text: published(index),
                                      style: const TextStyle(
                                        fontSize: 10,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    maxLines: 1,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child: Image.network(
                                  articles[index].urlToImage,
                                  fit: BoxFit.cover,
                                  height: double.infinity,
                                  errorBuilder: (_, __, ___) {
                                    return Container(
                                      color: Colors.grey[300],
                                      height: double.infinity,
                                      child: Icon(Icons.image),
                                    );
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          );
  }

  String published(int index) {
    DateTime dateTime = DateTime.parse(articles[index].publishedAt).toLocal();
    var difference = DateTime.now().difference(dateTime);
    if (difference.inDays > 7) {
      return 'a week ago';
    } else if (difference.inDays > 1) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays > 0) {
      return '${difference.inDays} day ago';
    } else if (difference.inHours > 1) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours > 0) {
      return '${difference.inHours} hour ago';
    } else if (difference.inMinutes > 0) {
      return '${difference.inMinutes} min ago';
    } else {
      return 'a moment ago';
    }
  }
}

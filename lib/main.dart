import 'package:flutter/material.dart';

import 'package:get_news_app/app/app.dart';

void main() {
  runApp(const App());
}
